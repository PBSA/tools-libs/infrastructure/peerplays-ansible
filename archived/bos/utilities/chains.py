known_chains = {
    "ALICE": {
        "chain_id": "6b6b5f0ce7a36d323768e534f3edb41c6d6332a541a95725b98e28d140850134",
        "core_symbol": "PPY",
        "prefix": "PPY",
    },
    "BEATRICE": {
        "chain_id": "b3f7fe1e5ad0d2deca40a626a4404524f78e65c3a48137551c33ea4e7c365672",
        "core_symbol": "TEST",
        "prefix": "TEST",
    },
    "TEST": {
        "chain_id": "63e4ed3747366527a485083b2e9ae8d7100d97bc132c7f28213b5439063981ef",
        "core_symbol": "TEST",
        "prefix": "TEST",
    }
}
